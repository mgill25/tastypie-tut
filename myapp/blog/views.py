# Create your views here.

# Django imports
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

# Local imports
from blog.models import Entry

