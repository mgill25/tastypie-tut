#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User

from blog.models import Entry

user = User()
user.id = 1

entry = Entry()
entry.title = "Test Entry"
entry.slug = "/test-entry"
entry.body = "This is a test entry. Hello, world! :)"
entry.save()
